package main

import (
	"io"
	"os"
	"os/exec"

	"github.com/urfave/cli"
)

const (
	pathPkg    = "app"
	pathGoSrc  = "/go/src"
	pathGoPkg  = pathGoSrc + "/" + pathPkg
	pathOutput = "/tmp/gosec.json"
	pathGosec  = "/usr/local/bin/gosec"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var cmd *exec.Cmd
	var err error

	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	// We don't control the directory where the source code is mounted
	// but Go requires the code to be within $GOPATH.
	// We could create a symlink but that wouldn't work with Gosec,
	// so we have to copy all the project source code
	// to some directory under $GOPATH/src.
	// TODO: make it possible to specify the exact path of the package.
	cmd = setupCmd(exec.Command("cp", "-r", path, pathGoPkg))
	err = cmd.Run()
	if err != nil {
		return nil, err
	}

	// NOTE: Gosec exit with status 1 if some vulnerabilities have been found.
	// This can be disabled by setting the -quiet flag but then
	// Gosec returns no output when it can't find any vulnerability.
	// See https://github.com/securego/gosec/blob/master/cmd/gosec/main.go
	cmd = setupCmd(exec.Command(pathGosec, "-fmt=json", "-out="+pathOutput, "./..."))
	cmd.Dir = pathGoPkg
	cmd.Run()
	return os.Open(pathOutput)
}
